#pragma once


class Action{
public:
    Action(){}
    virtual ~Action(){}
    virtual void operator()()=0;
    void toString(){
        std::cout << "do Action !" << std::endl;
    }
};



class NonObjectAction : public Action {
    void (*_function)(void);

public:
    NonObjectAction(void (*f) (void)):

            _function(f){}
    virtual void operator()(){ _function(); }
};

template <class C, typename R>
class ObjectAction : public Action{
public:
    ObjectAction(R(C::*f)(void), C& obj):
        _obj(&obj),
        _function(f){}
    virtual R operator()() {  (_obj->*_function)();}
private:
    C * _obj;
    R (C::*_function)( void);
};
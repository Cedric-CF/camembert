#pragma once

#include <FormeInstance.h>
#include <stdlib.h>
#include "glm/vec2.hpp"
#include "ActorViewManager.hpp"

class Actor {
public:


    Actor(FormeInstance* forme,
            float lifepoints = 1.,
            glm::vec2 direction = glm::vec2(0.,0.),
            glm::vec2 speed = glm::vec2(5,5)) :
            direction(direction) ,
            speed(speed),
            forme(forme),
            lifePoints(lifepoints){

    }

    virtual ~Actor(){
        if( forme != NULL)
            delete forme;
    }

    virtual void update() = 0;


    glm::vec2 direction;
    glm::vec2 position;
    glm::vec2 speed;

    FormeInstance * forme;

    float lifePoints;

    const glm::vec2 & getPosition()const;
    float getSize()const;

    static ActorViewManager* actorViewManager;

    float rotation; //TODO : use vector to define transformation

   virtual bool addDamages(float damages);

    //glm::vec2 rotation;
protected:




};
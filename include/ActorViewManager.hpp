#pragma once

#include <vector>
#include "FormeManager.h"
#include "FormeModele.h"
#include "FormeInstance.h"
#include "Shader.h"
#include <string>
static std::string const vertexShader = "Shaders/forme2D.vert";
static std::string const fragmentShader = "Shaders/forme2D.frag";

class ActorViewManager{
    Shader _shader;
    std::vector<FormeModele *> _listModels;
    std::map<std::string, glm::vec3> _listColor;
    FormeManager _formeManager;

public:
    ActorViewManager(float width, float height);
    void update();
    FormeInstance * generatePlayer(glm::vec2 position, float angle);
    FormeInstance * generateOpponant(glm::vec2 position, float angle);
    FormeInstance * generateBulletPlayer(glm::vec2 position, float angle);
};
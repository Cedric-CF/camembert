#pragma once

#include "Actor.h"
#include "ActorViewManager.hpp"
#include "Controller.hpp"

class Bullet : public Actor {


    float _power;
public:
    Bullet(
            const glm::vec2 & position,
            float power = 5.,
            glm::vec2 direction = glm::vec2(0.,1.),
            glm::vec2 speed = glm::vec2(1.,1.)) :
            Actor(actorViewManager->generateBulletPlayer(position, 15), 1.,direction, speed),
            _power(power)
    {
        speed = glm::vec2(50,50);;

    }

    virtual void update();
    virtual ~Bullet(){
    }

    float getPower(){
        return _power;
    }
};


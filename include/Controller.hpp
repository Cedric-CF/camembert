#pragma once


#include <SDL2/SDL_scancode.h>

class Controller{
    SDL_Scancode _actionKey;
    SDL_Scancode _rightKey,_leftKey, _bottomKey, _topKey;

public:
    Controller();

    int getHorizontalInput();
    int getVerticalInput();
    bool getAction();
};
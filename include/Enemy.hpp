#pragma once

#include "Actor.h"
#include "ActorViewManager.hpp"
#include "Controller.hpp"

class Enemy : public Actor {
    static int count;
    int i;
    glm::vec2 (*deplacementFunction) (glm::vec2, glm::vec2);
public:

    bool _isDeleting;

    Enemy(const glm::vec2 & position, float lifepoint = 5.) :
            Actor(actorViewManager->generateOpponant(position, 15), lifepoint),
            i(count++),
            _isDeleting(false)
    {
        speed = glm::vec2(0,3);
        forme->setAngle(60);
        //std::cout << position.x << ";" << position.y << " ; " << i << std::endl;
    }

    virtual ~Enemy(){
    }
    virtual void update();
};

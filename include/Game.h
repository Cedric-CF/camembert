#pragma once

#include <chrono>

class Game {
public:
    static float getDt() { return dt; }
    static float updateDt();
private:
    static float dt; //delta time between two frames, in seconds
    static std::chrono::time_point<std::chrono::high_resolution_clock> last;
};
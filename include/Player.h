#pragma once

#include "Actor.h"
#include "ActorViewManager.hpp"
#include "Controller.hpp"

class Player : public Actor {

    Controller &_oontroller;


public:
    bool _firing;
    Player(const glm::vec2 & position, Controller & controller) :
            Actor(actorViewManager->generatePlayer(position, 15)),
            _oontroller(controller),
            _firing(false)
    {
        speed = glm::vec2(100,100);
        forme->setAngle(60);

    }

    virtual ~Player(){
        static int i =0;
        std::cout << "Delete Player : " <<i++<< std::endl;
    }
    virtual void update();
};


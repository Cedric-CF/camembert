#pragma once
#include "Actor.h"
#include "Player.h"
#include "ActorViewManager.hpp"
#include "Enemy.hpp"
#include "Bullet.hpp"
#include "Timeline.hpp"

class Stage{
    Controller _controller;
    Player _player;
    std::list<Enemy> _enemies;
    std::list<Bullet> _playerBullet;
    glm::mat2 _playableZone;

    Timeline _mainTimeline;

    bool isOutScreen(Actor& a1);
    bool isInContact(Actor& a1, Actor& a2);
    void collideEnnemiesWithBullet();
public:

    Stage();
    virtual ~Stage();
    void init();
    void update();
    void generateEnemyWave();

};


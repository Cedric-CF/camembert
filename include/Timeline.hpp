#pragma once

#include "Action.hpp"
#include <map>
#include <memory>

class Timeline{

    std::map<float, Action *> _listActions;

    float _firstTick, _maxDuration,_currentTick, _tickRate;

public:
    Timeline(float firstTick = 0, float maxDuration = -1, float maxFrame = -1, float currentTick = 0, float tickRate = 1):
            _listActions(),
            _firstTick(firstTick),
            _maxDuration(maxDuration),
            _currentTick(currentTick)

    {
        if(maxDuration != -1)
            tickRate = 1;

    }

    void update(){
        _currentTick += 1.;

        auto it = _listActions.find(_currentTick);
        if(it != _listActions.end())
            (*(it->second))();
    }

    void add(Action *a, double flagTime ){
        std::cout << "Add Action" << std::endl;
        _listActions.insert(std::make_pair(flagTime,a));
    }

    void remove(float flagTime){
        //TODO: Suppression d'un �l�ment � ce cr�neau l�
    }

    void remove(float beginTime, float endTime){
        //TODO: Suppression
    }

};
#include <Game.h>
#include "Actor.h"

ActorViewManager * Actor::actorViewManager = nullptr;

const glm::vec2 &Actor::getPosition() const {
    return forme->getPosition();
}

float Actor::getSize() const {
    return forme->getSize();
}

bool Actor::addDamages(float damages) {
    lifePoints -= damages;
    return (lifePoints <= 0);
}

#include "ActorViewManager.hpp"

using  namespace std;

ActorViewManager::ActorViewManager(float width, float height)
        :_formeManager(width, height)
{

    _shader = Shader("Shaders/forme2D.vert", "Shaders/forme2D.frag");


    for(int i = 1; i < 10; ++i)
    _listModels.emplace_back(_formeManager.generateRegularPolygone(i));

    _listColor.emplace("Red", glm::vec3(255,0.1,0.1));
    _listColor.emplace("Blue", glm::vec3(.1,.1,1.0));
    _listColor.emplace("Green", glm::vec3(.1,255,.1));
    _listColor.emplace("Yellow", glm::vec3(1.0,1.0,.1));

    _formeManager.newCollection(string("Player"), _shader.getProgramID(), *_listModels.at(2));
    _formeManager.newCollection(string("Ennemi-01"), _shader.getProgramID(), *_listModels.at(2));
    _formeManager.newCollection(string("Bullet-01"), _shader.getProgramID(), *_listModels.at(8));
}


void ActorViewManager::update() {
    glClear(GL_COLOR_BUFFER_BIT);
    _formeManager.display();
}

FormeInstance * ActorViewManager::generatePlayer(glm::vec2 position, float angle) {
    return _formeManager.newObject("Player", position, _listColor.at("Green"), 5, 2);
}
FormeInstance * ActorViewManager::generateOpponant(glm::vec2 position, float angle){
    return _formeManager.newObject("Ennemi-01", position, _listColor.at("Red"), 4, 0);
}

FormeInstance *ActorViewManager::generateBulletPlayer(glm::vec2 position, float angle) {
    return _formeManager.newObject("Bullet-01", position, _listColor.at("Yellow"), 1, 0);
}


#include "Bullet.hpp"

void Bullet::update() {
    if(direction.x == 0 && direction.y == 0)
        direction.x = 1;
    direction = normalize(direction);
    glm::vec2 p = speed * direction;
    forme->translate(p.x, p.y);
}
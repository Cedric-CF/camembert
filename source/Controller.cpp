#include <Input.h>
#include "Controller.hpp"

Controller::Controller():
    _actionKey(SDL_SCANCODE_SPACE),
    _rightKey(SDL_SCANCODE_RIGHT),
    _leftKey(SDL_SCANCODE_LEFT),
    _bottomKey(SDL_SCANCODE_DOWN),
    _topKey(SDL_SCANCODE_UP)
{

}
int Controller::getHorizontalInput(){
    return Input::keyPressed(_rightKey) - Input::keyPressed(_leftKey);
}

int Controller::getVerticalInput(){
    return Input::keyPressed(_bottomKey) - Input::keyPressed(_topKey);
}

bool Controller::getAction(){
    return Input::keyPressed(_actionKey);
}
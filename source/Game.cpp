#include <chrono>
#include "Game.h"

using namespace std;

std::chrono::time_point<std::chrono::high_resolution_clock> Game::last = std::chrono::high_resolution_clock::now() ;
float Game::dt = .1f;

float Game::updateDt() {
    auto now = std::chrono::high_resolution_clock::now();
    dt =  std::chrono::duration<float>(now - last).count();
    last = now;
}
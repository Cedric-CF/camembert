#include <Input.h>
#include "string.h"

Input Input::s_instance;

Input::Input():
m_winClosed(false),
m_mouseX(0),
m_mouseY(0),
m_mouseXRel(0),
m_mouseYRel(0){
	memset(m_keybd, 0, SDL_NUM_SCANCODES*sizeof(m_keybd[0]));
	memset(m_mouseBtn, 0, 8*sizeof(m_mouseBtn[0]));
}

Input::~Input() {
}

void Input::refresh() {
	while(SDL_PollEvent(&s_instance.m_event)){
		switch(s_instance.m_event.type){
			case SDL_KEYDOWN:
				s_instance.m_keybd[s_instance.m_event.key.keysym.scancode] = true;
				break;
			case SDL_KEYUP:
				s_instance.m_keybd[s_instance.m_event.key.keysym.scancode] = false;
				break;
			case SDL_MOUSEBUTTONDOWN:
				s_instance.m_keybd[s_instance.m_event.button.button] = true;
				break;
			case SDL_MOUSEBUTTONUP:
				s_instance.m_keybd[s_instance.m_event.button.button] = false;
				break;
			case SDL_MOUSEMOTION:
				s_instance.m_mouseX = s_instance.m_event.motion.x;
				s_instance.m_mouseY = s_instance.m_event.motion.y;
				s_instance.m_mouseXRel = s_instance.m_event.motion.xrel;
				s_instance.m_mouseYRel = s_instance.m_event.motion.yrel;
				break;
			case SDL_WINDOWEVENT:
				if(s_instance.m_event.window.event == SDL_WINDOWEVENT_CLOSE)
					s_instance.m_winClosed = true;
			default:
				break;
		}
	}
}

bool Input::btnPressed(unsigned char btn) {
	return s_instance.m_mouseBtn[btn];
}

bool Input::keyPressed(SDL_Scancode key) {
	return s_instance.m_keybd[key];
}

void Input::getMousePos(int* x, int* y) {
	*x = s_instance.m_mouseX;
	*y = s_instance.m_mouseY;
}

void Input::getMouseRelPos(int* x, int* y) {
	*x = s_instance.m_mouseXRel;
	*y = s_instance.m_mouseYRel;
}

bool Input::mouseMoved() {
	return (s_instance.m_mouseXRel != 0 || s_instance.m_mouseYRel != 0);
}

bool Input::windowClosed() {
	return s_instance.m_winClosed;
}

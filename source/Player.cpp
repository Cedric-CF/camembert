#include "Player.h"
#include "Game.h"

using namespace glm;

void Player::update() {


    // Move player
    vec2 direction( _oontroller.getHorizontalInput(), _oontroller.getVerticalInput());

    if((direction.x == 1) && (direction.y == 1))
        direction = glm::normalize(direction);


    vec2 position = forme->getPosition();
    position += Game::getDt() * speed * direction;
    position = max(position, getSize());
    position = min(position, vec2(800, 600) - getSize());
    forme->setPosition(position);

    // Detect fireAction

    _firing = _oontroller.getAction();

}

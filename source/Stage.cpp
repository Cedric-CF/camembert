#include <memory>
#include "Stage.hpp"
#include "Timeline.hpp"

void printHello(){
    std::cout<< "Hello World ! " << std::endl;
}

Stage::Stage():
    _controller(),
    _player(glm::vec2(400,550),_controller),
    _playableZone(0.,800.,0.,600.),
    _mainTimeline()
{

    srand(0);


    /*
    ObjectAction<Stage, void> addWave(&Stage::generateEnemyWave, *this);
    _mainTimeline.add(addWave, 200);
    _mainTimeline.add(addWave, 250);
    _mainTimeline.add(addWave, 450);
    _mainTimeline.add(addWave, 850);
    */

    // TODO : liste d'action + gestion de la m�moire
    NonObjectAction *addPrint = new NonObjectAction( &printHello );
    ObjectAction<Stage, void> *addWave = new ObjectAction<Stage, void>(&Stage::generateEnemyWave, *this);


    _mainTimeline.add(addPrint, (float)200.);
    _mainTimeline.add(addWave, (float)250.);
    _mainTimeline.add(addWave, (float)50.);
    _mainTimeline.add(addWave, (float)150.);


}



Stage::~Stage() {

}

void Stage::init() {

}

void Stage::generateEnemyWave(){
    for(int m = 0; m < 10; ++m)
        _enemies.emplace_back(glm::vec2(rand() % 800, -20));
}
void Stage::update() {
    std::vector<std::list<Enemy>::iterator> enemyToDestroy;
    static int i = 0;
    static int f = 0;
    i = (i + 1) % 3;
    f = (f + 1) % 2;
    if(i == 0){
        //generateEnemyWave();

    }

    _mainTimeline.update();
    _player.update();

    if( (_player._firing ) && (f == 0)) {
        //std::cout << "Fire ! " << std::endl;
        for(float m = -1; m <= 1; m+= 0.25 ){
            for(float n = -1; n <= 1; n+= 0.25 ){
                _playerBullet.emplace_back(_player.getPosition(),10, glm::vec2(m,n), glm::vec2(15.0,15.0));

            }
        }
    }


    Actor::actorViewManager->update();

    collideEnnemiesWithBullet();

    for(std::list<Enemy>::iterator enemy =_enemies.begin(); enemy != _enemies.end(); ++enemy ) {
        enemy->update();
        if(enemy->getPosition().y > _playableZone[1][1] + 20 ){
            enemyToDestroy.push_back(enemy);
        }
    }

    for(auto & bullet : _playerBullet)
        bullet.update();

    for(std::vector<std::list<Enemy>::iterator>::iterator enemy = enemyToDestroy.begin(); enemy != enemyToDestroy.end(); ++enemy)
    {
        //delete (*enemy)->forme;
        _enemies.erase(*enemy);
    }

    //std::cout << "Nb Enemis : "<< _enemies.size() << " | "<< "Nb Bullets : " << _playerBullet.size() << std::endl;


}

bool Stage::isInContact(Actor &a1, Actor& a2) {
    glm::vec2 a = a1.getPosition() - a2.getPosition();
    float d = a.x * a.x + a.y * a.y;
    float r = a1.getSize() + a2.getSize()*2;


    return (d <= r * r);
}
bool Stage::isOutScreen(Actor& a1){
    glm::vec2 p = a1.getPosition();
    return (
            (p.x < _playableZone[0][0]) || (p.x > _playableZone[0][1])||
            (p.y < _playableZone[1][0]) || (p.y > _playableZone[1][1]));



}
void Stage::collideEnnemiesWithBullet() {
    std::list<Bullet>::iterator itB;
    std::list<Enemy>::iterator itE;
    std::vector<std::list<Enemy>::iterator> enemyToDestroy;
    std::vector<std::list<Bullet>::iterator> toDestroy;

    for(itB = _playerBullet.begin(); itB != _playerBullet.end(); ++itB){
        if(isOutScreen(*itB)) {
            toDestroy.push_back(itB);
        }
        else{
            itE = _enemies.begin();
            while( (itE != _enemies.end())&&( !isInContact(*itB, *itE) )){
                ++itE;
            }

            if((itE != _enemies.end()) && (!itE->_isDeleting)){
                toDestroy.push_back(itB);
                if(itE->addDamages(itB->getPower())){
                    enemyToDestroy.push_back(itE);
                    itE->_isDeleting = true;
                }
            }
        }
    }

    for(std::vector<std::list<Bullet>::iterator>::iterator bullet = toDestroy.begin(); bullet != toDestroy.end(); ++bullet){
       // delete (*bullet)->forme;
        _playerBullet.erase(*bullet);
    }

    for(std::vector<std::list<Enemy>::iterator>::iterator enemy = enemyToDestroy.begin(); enemy != enemyToDestroy.end(); ++enemy)
    {
        //delete (*enemy)->forme;
        _enemies.erase(*enemy);
    }

}

#include <Actor.h>
#include <Stage.hpp>
#include "SceneSDL.h"
#include "Input.h"
#include "ActorViewManager.hpp"
#include "Shader.h"

int main(int argc, char** argv) {
    unsigned int
            debutBoucle(0), // Temps mesur� au d�but de la boucle
            finBoucle(0), 	// Temps mesur� � la fin
            tempsEcoule(0);
    float framerate(1000/60);

    SceneSDL scene("Camembert, 50,50,600,800");

    scene.init();


    ActorViewManager viewManager(800,600);

    Actor::actorViewManager = &viewManager;

    Stage firstLevel;


    while(!Input::keyPressed(SDL_SCANCODE_ESCAPE)){
        debutBoucle = SDL_GetTicks();

        Input::refresh();

        firstLevel.update();
        viewManager.update();

        scene.display();

        finBoucle = SDL_GetTicks();
        tempsEcoule = finBoucle - debutBoucle;
        // Si n�cessaire, on met en pause le programme
        if(tempsEcoule < framerate)
            SDL_Delay(framerate - tempsEcoule);
    }

    scene.display();


    scene.destroy();
    return 1;
}